import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("Hey");

        try(final WebClient webClient = new WebClient()) {
            HtmlPage page = webClient.getPage("http://htmlunit.sourceforge.net");
            String pageAsText = page.asText();
            System.out.println(pageAsText.substring(0, 40));
        }

    }

}
